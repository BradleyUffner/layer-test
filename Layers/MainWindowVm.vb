﻿Imports System.Collections.ObjectModel

Public Class MainWindowVm
    'Inherits ViewModelBase

    Public Sub New()
        Layers = New ObservableCollection(Of Layer)
        Layers.Add(New Layer(0, Brushes.Black))
        Layers.Add(New Layer(1, Brushes.Yellow))
        Layers.Add(New Layer(2, Brushes.Blue))
        Layers.Add(New Layer(3, Brushes.Red))

    End Sub

    Public Property Layers As ObservableCollection(Of Layer)

End Class
