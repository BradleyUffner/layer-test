﻿Public Class Check
    Public Property Value As Boolean
    Public Property Layer As Layer

    Public Sub New(layer As Layer)
        Me.Layer = layer
    End Sub

    Public ReadOnly Property BorderSize As Thickness
        Get
            Dim coords As Point = Layer.GetCoords(Me)
            Dim border As New Thickness(2, 2, 2, 2)

            If coords.X = 0 Then
                border.Left = 4
            End If
            If coords.Y = 0 Then
                border.Top = 4
            End If

            If coords.X = 3 Then
                border.Right = 0
                border.Top = 0
                border.Bottom = 0
            End If
            If coords.Y = 3 Then
                border.Bottom = 0
                border.Left = 0
                border.Right = 0
            End If

            Return border

        End Get
    End Property
End Class
