﻿Public Class WpfHelper
    Public Shared Function FindParent(Of T As DependencyObject)(child As DependencyObject) As T
        'get parent item
        Dim parentObject As DependencyObject = VisualTreeHelper.GetParent(child)

        'we've reached the end of the tree
        If parentObject Is Nothing Then
            Return Nothing
        End If

        'check if the parent matches the type we're looking for
        Dim parent As T = TryCast(parentObject, T)
        If parent IsNot Nothing Then
            Return parent
        Else
            Return FindParent(Of T)(parentObject)
        End If
    End Function
End Class
