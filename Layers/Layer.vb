﻿Imports System.Collections.ObjectModel

Public Class Layer

    Public Sub New(layerIndex As Integer, color As Brush)
        Me.LayerIndex = layerIndex
        Bits = New ObservableCollection(Of Check)
        For i As Integer = 1 To 16
            Bits.Add(New Check(Me))
        Next
        Me.Color = color
    End Sub

    Public Property Bits As ObservableCollection(Of Check)

    Private mLayerIndex As Integer
    Public Property LayerIndex As Integer
        Get
            Return mLayerIndex
        End Get
        Set(ByVal Value As Integer)
            If (mLayerIndex = Value) Then Return
            mLayerIndex = Value
            ' NotifyChange("LayerIndex")
        End Set
    End Property

    Private mRows As Integer=4
    Public Property Rows As Integer
        Get
            Return mRows
        End Get
        Set(ByVal Value As Integer)
            If (mRows = Value) Then Return
            mRows = Value
            ' NotifyChange("Rows")
        End Set
    End Property

    Private mCols As Integer=4
    Public Property Cols As Integer
        Get
            Return mCols
        End Get
        Set(ByVal Value As Integer)
            If (mCols = Value) Then Return
            mCols = Value
            ' NotifyChange("Cols")
        End Set
    End Property

    Private mColor As Brush
    Public Property Color As Brush
        Get
            Return mColor
        End Get
        Set(ByVal Value As Brush)
            If (mColor Is Value) Then Return
            mColor = Value
            'NotifyChange("Color")
        End Set
    End Property
    
    Public Function GetCoords(check As Check) As Point
        Dim index As Integer = Bits.IndexOf(check)
        Return New Point(index Mod 4, index \ 4)
    End Function
    


    Public ReadOnly Property TLMargin As Thickness
        Get
            Return New Thickness(LayerIndex * 20 + 10, LayerIndex * 20 + 10, (Cols * 20) - (LayerIndex * 20), (Rows * 20) - (LayerIndex * 20))
        End Get
    End Property
End Class
